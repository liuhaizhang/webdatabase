说明

      在javaweb项目中，需要使用ajax将复选框的值的提交到后台，记录一下当时碰到的问题
正文
一、使用jQuery得到复选框的值

var checkID=[];
                $("input[name='checkbox']:checked").each(function(i){
                      checkID[i] = $(this).val();
                });

    1
    2
    3
    4

二、使用ajax异步提交

这里写图片描述
注意图中红框的traditional选项

它的含义是是否使用传统的方式浅层序列化

默认为false，此时传到后台接收不到

这里写图片描述

改为true后，后台可以正常接收到

这里写图片描述
三、完整代码

复选框

<input type="checkbox" value="${news.nid}" name="checkbox" id="checkbox">

    1

脚本

<script type="text/javascript">
            function tijiao(){
                var checkID=[];
                $("input[name='checkbox']:checked").each(function(i){
                      checkID[i] = $(this).val();
                });

                $.ajax(
                    {data:{'checkID':checkID},
                      dataType:'text',
                      success: function(data){
                        alert(data);
                      },
                      type:'post',
                      url:'<%=basePath%>AddHotNewsSvl',
                      traditional:true
                    }
                );
            }

     </script>

    1
    2
    3
    4
    5
    6
    7
    8
    9
    10
    11
    12
    13
    14
    15
    16
    17
    18
    19
    20
    21

后台

protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[]  checkID = request.getParameterValues("checkID");
        try {
            HotNewsIn.addHot(checkID);
        } catch (Exception e) {
            e.printStackTrace();
            Log.logger.error(e.getMessage());
        }
        response.setCharacterEncoding("utf-8");
        response.getWriter().write("设置成功");
    }
————————————————
版权声明：本文为CSDN博主「布碗」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/sinat_36553913/article/details/78418098