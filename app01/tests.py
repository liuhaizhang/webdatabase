import pymysql
from django.test import TestCase

# Create your tests here.


def MyPaginator(paginator,page_num,url):
    page_html=''
    if paginator.num_pages < 5:
        for i in range(1, paginator.num_pages + 1):
            if i == page_num:
                page_html += f'<li class="active"><a href="{url}?page= {i}" >{i}</a></li>'
            else:
                page_html += f'<li><a href="{url}?page= {i}" >{i}</a></li>'
    elif page_num < 5:
        for i in range(1, 6):
            if i == page_num:
                page_html += f'<li class="active"><a href="{url}?page= {i}" >{i}</a></li>'
            else:
                page_html += f'<li><a href="{url}?page= {i}" >{i}</a></li>'
    elif page_num > paginator.num_pages - 5:
        for i in range(paginator.num_pages - 4, paginator.num_pages + 1):
            if i == page_num:
                page_html += f'<li class="active"><a href="{url}?page= {i}" >{i}</a></li>'
            else:
                page_html += f'<li><a href="{url}?page= {i}" >{i}</a></li>'
    else:
        for i in range(page_num - 2, page_num + 3):
            if i == page_num:
                page_html += f'<li class="active"><a href="{url}?page= {i}" >{i}</a></li>'
            else:
                page_html += f'<li><a href="{url}?page= {i}" >{i}</a></li>'
    return page_html
import random
#
# def random_code():
#     upper = chr(random.randint(65,90))
#     lower = chr(random.randint(97,122))
#     num = random.randint(0,9)
#     code =''
#     for i in range(5):
#         code+=str(random.choice([upper,lower,num]))
#     return code
import os
#获取一级目录下的文件名，
def get_filename(path):

    lis=os.listdir(path)#拿到目录下所有的文件名或目录名
    file_lis=[]
    for file in lis:
        file_path = os.path.join(path,file)
        if os.path.isfile(file_path):
            file_lis.append(file)
    return file_lis
ret=get_filename(r'F:\installsotf\python\BBS14\media\article_img')

# for i in range(1,5):
#     for j in [1,2]:
#         if j==i:
#             print(j,'j')
# for i in range(1,5):
#    if i  in [1,2]:
#        print(i,'i')
from django.conf import settings
filr =os.path.join('F:/installsotf/python/BBS14/media/user_file','test01.txt')
print(filr)

from Utils import email
#
# email_obj = email.Send_mail(sender='13510391274@163.com',passward='IMJBAXPFAUJSXSUZ',receivers='2414155342@qq.com')
# name='五邑博客园@'
# text = '您的验证码是：hss7jk'
# head = '请确认是本人操作，不要将验证码提供给其他人。'
# email_obj.send(Name=name,ShowText=text,Header_show=head)


def random_code(n=5):
    upper = chr(random.randint(65,90))
    lower = chr(random.randint(97,122))
    num = str(random.randint(0,9))
    code =''

    for i in range(n):
        code+=random.choice([upper,lower,num])

    return code


# class Person():
#     def __init__(self ,name):
#         self.name=name
#     def __setitem__(self, key, value):
#         setattr(self,key,value)
# obj = Person('lhz')
# print(obj.name)
# obj['name']=10
# print(obj.name)

class Mydict(dict):
    def __getattr__(self, item):
        #item就是key值
        print('对象加点取值会触发我')
        return self[item]
    def __setattr__(self, key, value):
        print('对象加点赋值，会触发我')
        self[key]=value

my=Mydict(name='lhz',sex='male',age=13)
print(my.name)
my.age=22
print(my.age)

#重写上下文管理器，在with下写查询操作语句。

class Mysql:
    def __init__(self,port,user,host,password,database):
        import pymysql
        self.port=port
        self.user=user
        self.password=password
        self.host=host
        self.database=database
    def __enter__(self):
        #执行with时，先执行这个
        self.con = pymysql.connect(port=self.port,user=self.user,host=self.host,password=self.password,database=self.database)
        return self.con
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.con.close()
        #退出with时先关闭数据库连接
#
myq = Mysql(port=3306,user='root',password='Huawei@123',database='day37',host='127.0.0.1')
with myq as con:
    cursor = con.cursor(cursor=pymysql.cursors.DictCursor)
    sql='select * from student_msg'
    cursor.execute(sql)
    ret = cursor.fetchall()
    print(ret)

print('---------------------')


#
# try:
#     print('in try')
# except Exception as e:
#     print('in except')
# else:
#     print('in else')
#     ty
# finally:
#     print('in finally')
#自定义一个异常类
class NotStrException(BaseException):
    def __init__(self,msg):
        self.msg =msg
    def __str__(self):
        return self.msg

class Person(object):
    def __setattr__(self, key, value):
        # print(key,value,type(value)), 属性是给key，属性的值是传递给value的。
        if isinstance(value,str): #是对象，前面是后面的对象吗？是就是true，否则是false
            # issubclass() 前面是后面的子类吗，是就返回True，表示就是返回False
            # setattr(self,key,value) #内部原理 self.key=value,在这里不能使用，因为报最大递归错误。
            #1、将其包装成字典的形式，进行保存值
            # self.__dict__[key]=value
            #2、调用父类object的__setattr__方法
            object.__setattr__(self,key,value)
        else:
            raise NotStrException('必须是字符串') #抛出自定义的异常

# per = Person()
# try:
#     # per.name = 123
#     per.name = '123'
# except NotStrException as e:
#     print(e)
#
# print(per.name)