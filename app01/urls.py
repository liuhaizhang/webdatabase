from django.urls import path, re_path
from . import views

# from app01 import search_views

urlpatterns = [
    # 注册
    path('register/', views.register, name='app01_register'),
    # 登录
    path('login/', views.login, name='app01_login'),
    # 主页面
    path('home/', views.home, name='app01_home'),
    # 显示文件资源的页面
    path('file_home/', views.file_home, name='app01_file_home'),
    # 获取验证码图片
    path('get_code/', views.get_code, name='app01_get_code'),
    # 退出登录
    path('logout/', views.logout, name='app01_logout'),
    # 修改密码
    path('set_password/', views.set_password, name='app01_set_pwd'),
    # 个人站点
    path('site/<str:username>/', views.site, name='app01_site'),
    # 侧边栏筛选功能，多个url指向site的·视图函数，
    path('site/<str:username>/tag/<int:tag_id>/', views.site),
    path('site/<str:username>/category/<int:category_id>/', views.site),
    path('site/<str:username>/archive/<str:archive_month>/', views.site),
    #资源库的分类
    path('site/<str:username>/categorys/<int:web_source_id>/', views.site),
    # 两个视图数是一样的，那么视图函数接收的函数就是不确定了，要使用**kwargs或*args来接收
    # re_path('stite/(?P<username>\w+)/(?P<condition>tag|category|archive)/(?P<param>.*)/',views.site)
    # 文章详情页
    path('site/<str:username>/article/<int:article_id>/', views.article_detail, name='app01_article'),
    # 点赞或点踩功能
    path('up_or_down/', views.up_or_down, name='app01_up_or_down'),
    # 文章评论的url
    path('article/comment/', views.comment, name='app01_comment'),
    # 后台管理
    path('backend/', views.backend, name='app01_backend'),
    # 添加文章
    path('backend/add_article/', views.add_article, name='app01_add_article'),
    # 编辑器上传图片的接收路径
    path('upload/img/', views.upload_img, name="app01_uploadImg"),
    # 修改用户头像
    path('change_avatar/', views.chang_avatar, name='app01_change_avatar'),
    # 个人资料修改：
    path('change_datas/', views.change_datas, name='app01_change_datas'),
    #展示我的班级的所有人
    path('myclass/',views.MyClass,name='app01_myclass'),
    # 修改文章
    path('editor_article/', views.editor_article, name='app01_editor_article'),
    # 文件上传
    path('upload_file/', views.upload_file, name='app01_upload_file'),
    # 操作文章
    path('operate_file/', views.operate_file, name='app01_operate_file'),
    # 文件下载
    path('download/file/', views.download_file, name='app01_download_file'),
    # 文件预览
    path('preview_file/', views.preview_file, name='app01_preview'),
    # 文章的收藏
    path('cellct_article/', views.cellact_article, name='app01_cellact_article'),
    # 文件收藏展示
    path('file_cellation/', views.file_cellation, name='app01_fi_cellation'),
    # 文章收藏展示
    path('article_cellation', views.article_cellation, name='app01_ar_cellation'),
    # 找回密码的路由
    path('find_password/', views.find_password, name='app01_find_password'),
    # 搜索的
    path('search/', views.search, name='app01_search'),
    # 个人站点上的文件显示
    path('site/<str:username>/file/', views.site_file, name="app01_site_file"),
    # 关注的视图函数
    path('focu/user/', views.focu_user, name="app01_focu_user"),
    # 我的关注
    path('my_focu/', views.my_focu, name='app01_my_focu'),
    # 管理员的账户
    path('admin/', views.admin_home, name='app01_admin_home'),
    path('admin/teacher/', views.admin_teacher, name='app01_admin_teacher'),
    path('admin/article/', views.admin_article, name='app01_admin_article'),
    path('admin/files/', views.admin_file, name='app01_admin_file'),
    #上传可以管理员批量导入用户的文件
    path('admin/upload/',views.admin_upload,name = 'app01_admin_upload'),
    #展示管理员文件，并可以批量导入
    path('admin/upload/files/', views.admin_upload_files, name='app01_admin_upload_files'),
    #批量导入用户的
    path('admin/add/users/',views.admin_add_users,name='app01_admin_add_users'),
    path('admin/delete/',views.delete),
    #修改管理员信息
    path('change_admin/',views.change_admin,name='app01_change_admin'),
    #增加管理员账户
    path('add_admin/',views.add_admin,name='app01_add_admin'),
    #显示所有的管理员
    path('show_admin/',views.show_admin,name='app01_show_admin'),
    #临时设置班级的
    path('set/',views.set),
    #增加单个用户
    path('admin_add_user/',views.admin_add_user,name='app01_admin_add_user'),
    #老师管理学生
    path('manage_student/',views.manage_student,name="app01_manage_stu"),
    #私信的url
    path('userwechat/',views.userWechat,name='app01_wechat'),
    #未读消息
    path('message/',views.message,name='app01_message'),
    path('admin_user_comment/',views.admin_user_comment),
    #web资源分类
    path('web_sourse/',views.web_sourse,name='app01_web_sourse'),
    #上传web资源
    path('up_web_source/',views.up_web_source,name='app01_up_web_source'),
    #展示资源的url,下载原代码
    path('show_web_source/',views.show_web_source,name='app01_show_web'),
    #展示所有资源的页面
    path('web_home/',views.web_home,name='app01_web_home'),
    #资源详细展示出来
    path('site/<str:username>/source_detail/<int:sou_pk>/',views.source_detail,name='app01_source_detail'),
    #资源收藏
    path('source/collection/',views.source_collection,name='app01_sou_colle'),
    #管理用户的资源
    path('admin/source/',views.admin_source,name='app01_admin_source'),
    path('editer/source/',views.editer_source,name='app01_editer_source'),



]
