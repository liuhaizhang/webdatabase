from django import template
from django.db.models import Count
from django.db.models.functions import TruncMonth

register=template.Library()
from app01 import models


#自定义inclusion_tag，个人站点中左边筛选框
@register.inclusion_tag('app01/left_menu.html')
def left_menu(username,request):
    username=username
    #构造侧边栏需要的数据
    blog = models.Blog.objects.filter(site_name=username).first()
    # 查询站点的分类，及分类的文章数
    category_obj = models.Category.objects.filter(blog=blog.id,article__is_active=True).annotate(count_num=Count('article__id')).values('name', 'count_num','id')
    # 查询站点的标签，及标签的文章数
    tag_obj = models.Tag.objects.filter(blog=blog,article__is_active=True).annotate(count_num=Count('article__id')).values('name', 'count_num','id')
    # 查询站点，以创建时间分类的文章数、创建时间
    time_obj = models.Article.objects.filter(blog=blog).annotate(month=TruncMonth('create_time')).values('month').annotate(article_count=Count('id')).values('month', 'article_count')
    #查询用户上传的所有资源
    category_web = models.Categorys.objects.filter(userinfo__username=username)
    user_file = models.UserFile.objects.filter(userinfo__username=username)
    return locals()


#web资源展示左边的推荐
@register.inclusion_tag('app01/web_sourse/home_left.html')
def home_left(request):
    good_source =models.Resource.objects.filter(is_delete=False).order_by('-down_numbers')[:5]
    good_article = models.Article.objects.filter(is_active=True).order_by('-up_num')[:5]
    return locals()

#web资源展示右边的推荐
@register.inclusion_tag('app01/web_sourse/home_right.html')
def home_right(request):
    new_source = models.Resource.objects.filter(is_delete=False).order_by('-create_time')[:5]
    new_article = models.Article.objects.filter(is_active=True).order_by('-create_time')[:5]
    return locals()

#帖子展示左边推荐
@register.inclusion_tag('app01/web_sourse/home_article_left.html')
def article_left(request):
    good_source =models.Resource.objects.filter(is_delete=False).order_by('-down_numbers')[:5]
    good_article = models.Article.objects.filter(is_active=True).order_by('-up_num')[:5]
    return locals()


#帖子转时右边推荐
@register.inclusion_tag('app01/web_sourse/home_article_right.html')
def article_right(request):
    new_source = models.Resource.objects.filter(is_delete=False).order_by('-create_time')[:5]
    new_article = models.Article.objects.filter(is_active=True).order_by('-create_time')[:5]
    return locals()

#笔记展示时，推荐的内容
@register.inclusion_tag('app01/web_sourse/home_bote_left.html')
def bote_left(request):
    good_file = models.UserFile.objects.filter(is_active=True).order_by('-number')[0:5]
    good_source = models.Resource.objects.filter(is_delete=False).order_by('-down_numbers')[:5]
    return locals()
#笔记展示时，推荐的内容
@register.inclusion_tag('app01/web_sourse/home_bote_right.html')
def bote_right(request):
    new_file = models.UserFile.objects.filter(is_active=True).order_by('-create_time')[0:5]
    new_source = models.Resource.objects.filter(is_delete=False).order_by('-create_time')[0:5]
    return locals()


@register.inclusion_tag('app01/change_avatar.html')
def change_avatar():
    pass


