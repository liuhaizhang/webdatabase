# Generated by Django 2.2.12 on 2021-10-26 03:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0003_auto_20211026_0829'),
    ]

    operations = [
        migrations.AddField(
            model_name='userfile',
            name='create_time',
            field=models.DateField(auto_now_add=True, null=True),
        ),
    ]
