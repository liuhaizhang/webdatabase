# Generated by Django 2.2.12 on 2021-11-05 00:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0017_auto_20211103_2303'),
    ]

    operations = [
        migrations.AddField(
            model_name='userinfo',
            name='limit',
            field=models.BooleanField(default=0),
        ),
    ]
