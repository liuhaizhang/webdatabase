# Generated by Django 2.2.12 on 2021-11-19 12:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0027_resource_down_numbers'),
    ]

    operations = [
        migrations.AddField(
            model_name='resource',
            name='create_time',
            field=models.DateField(auto_now_add=True, null=True),
        ),
    ]
