# Generated by Django 2.2.12 on 2021-11-18 16:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0024_auto_20211118_2359'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resource',
            name='level',
            field=models.IntegerField(choices=[(1, '高级'), (2, '中级'), (3, '初级')], default=3),
        ),
    ]
