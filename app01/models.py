from django.db import models

# Create your models here.
'''
先写普通字段
之后再写外键字段
'''

from django.contrib.auth.models import AbstractUser

#站点表

class Blog(models.Model):
    site_name=models.CharField(verbose_name='站点名称',max_length=32)
    site_title = models.CharField(verbose_name='站点标题', max_length=32,default='这人很懒，什么都没有留下')
    #简单卖模拟，认识样式内部原理的操作,保存样式文件路径
    site_theme=models.CharField('站点样式',max_length=64)
    # class Meta:
    #     verbose_name_plural='站点表'
    def __str__(self):
        return self.site_name

#用户
class UserInfo(AbstractUser):
    phone = models.BigIntegerField(verbose_name='手机号',null=True,blank=True)
    #头像
    avatar = models.FileField(upload_to='avatar/',default='avatar/default.jpg')
    '''
    给avatar字段传外键对象，该文件会自动存储到media/avatar，用户不上传就默认使用图片。
    '''
    create_time = models.DateField(auto_now_add=True)
    #用户与站点是一对一关系,先设置可为空，因为站点没有那么快生成，只有方便创建用户
    blog = models.OneToOneField(to=Blog,null=True,on_delete=models.CASCADE)
    #用户与上传的文件资源的关系是一对多。外键在多上
    #用户与用户的关注问题是多对多关系
    friends = models.ManyToManyField(to='self', through='User2User', through_fields=('fans_user', 'star_user'), symmetrical=False)
    is_active = models.BooleanField(default=1)
    #禁言的字段,1代表被禁言了
    limit = models.BooleanField(default=0)
    #用户班级
    stu_class = models.CharField(max_length=30,default='200732')
    # class Meta:
    #     verbose_name_plural = '用户表'
    def __str__(self):
        return self.username

#用一张表来记录用户的关注问题
class User2User(models.Model):
    fans_user = models.ForeignKey(to=UserInfo, related_name="fans_user", on_delete=models.CASCADE)
    star_user = models.ForeignKey(to=UserInfo, related_name='star_user', on_delete=models.CASCADE)
    crete_time = models.DateField(auto_now_add=True,null=True)

 #文章分类表
class Category(models.Model):
    name = models.CharField(verbose_name='文章分类',max_length=32)
    #站点与分类是一对多。
    blog = models.ForeignKey(to=Blog,on_delete=models.CASCADE,null=True)

    # class Meta:
    #     verbose_name_plural = '文章分类表'
    def __str__(self):
        return self.name

#标签
class Tag(models.Model):
    name = models.CharField(verbose_name='文章标签',max_length=32)
    #站点与标签是一对多
    blog = models.ForeignKey(to=Blog, on_delete=models.CASCADE, null=True)

    # class Meta:
    #     verbose_name_plural = '文章标签表'
    def __str__(self):
        return self.name




class Article(models.Model):
    title = models.CharField(verbose_name='文章标题',max_length=64)
    desc = models.CharField(verbose_name='文章简介',max_length=256)
    content =models.TextField(verbose_name='文章内容')
    create_time = models.DateField(verbose_name='创建时间',auto_now_add=True) #创建时记录时间

    #数据库字段优化设计
    up_num = models.BigIntegerField(verbose_name='点赞数',default=0)
    down_num = models.BigIntegerField(verbose_name='点踩',default=0)
    comment_num=models.BigIntegerField(verbose_name='评论数',default=0)

    #外键字段
    #站点与文章是一对多
    blog = models.ForeignKey(to=Blog, on_delete=models.CASCADE, null=True)
    #文章与标签是多对多，三种创建方式，半自动
    tags = models.ManyToManyField(to=Tag,through='Article2Tag',through_fields=('article','tag')) #表顺序
    #分类与文章1对多
    category = models.ForeignKey(to=Category,on_delete=models.CASCADE,null=True)
    #代表文章的状态,删除与否
    is_active = models.BooleanField(default=1)

    # class Meta:
    #     verbose_name_plural='文章表' #修改admin后台管理默认的表名
    def __str__(self):
        return self.title
class UserFile(models.Model):
    name=models.CharField(verbose_name='文件名',max_length=50,)
    filename = models.CharField(verbose_name='文件带后缀的名字',max_length=50,null=True)
    share = models.BooleanField(default=1,verbose_name='可共享')
    desc = models.CharField(verbose_name='文件描述',max_length=200,null=True)
    #用户上传的共享文件保存到，media文件夹中的user_file文件夹中
    file = models.FileField(upload_to='user_file/')
    #用户上传文件的时间
    create_time = models.DateField(auto_now_add=True,null=True)
    #该文件被下载的次数
    number = models.IntegerField(default=0)
    #标志文件是否删除了
    is_active = models.BooleanField(verbose_name='是否活跃',default=1)
    #用户与上传的文件的关系是一对多的关系，所以外键在多上
    userinfo = models.ForeignKey(to=UserInfo,on_delete=models.CASCADE)

class AdminFile(models.Model):
    name = models.CharField(verbose_name='文件名', max_length=50, null=True)
    filename = models.CharField(verbose_name='文件带后缀的名字', max_length=50, null=True)
    file = models.FileField(upload_to='admin_file/')
    is_active = models.BooleanField(default=1)
    #该文件是否被导入过
    status = models.BooleanField(default=0)
    userinfo = models.ForeignKey(to=UserInfo, on_delete=models.CASCADE)

#文章与标签的第三章表，半自动
class Article2Tag(models.Model):
    article = models.ForeignKey(to=Article,on_delete=models.CASCADE)
    tag = models.ForeignKey(to=Tag,on_delete=models.CASCADE)

    # class Meta:
    #     verbose_name_plural = '文章点赞表'



#点赞点踩表
class UpAndDown(models.Model):
    user = models.ForeignKey(to=UserInfo,on_delete=models.CASCADE)
    article = models.ForeignKey(to=Article,on_delete=models.CASCADE)
    is_up = models.BooleanField() #传布尔值，存0和1.

    # class Meta:
    #     verbose_name_plural = '点赞点踩表'
#
#评论表
class Comment(models.Model):
    user = models.ForeignKey(to=UserInfo, on_delete=models.CASCADE)
    article = models.ForeignKey(to=Article, on_delete=models.CASCADE)
    #评论内容
    content =models.CharField(verbose_name='评论内容',max_length=256)
    comment_time = models.DateTimeField(verbose_name='评论时间',auto_now_add=True)
    #自关联，
    parent = models.ForeignKey(to='self',on_delete=models.CASCADE,null=True) #有些论是根评论
    # is_delete = models.BooleanField(default=False)
    # class Meta:
    #     verbose_name_plural = '评论表'

#用户与文件收藏是多对多的关系,用第三张表来表示
class User2Article(models.Model):
    user = models.ForeignKey(to=UserInfo,on_delete=models.CASCADE)
    article = models.ForeignKey(to=Article,on_delete=models.CASCADE)
    #收藏状态
    static= models.BooleanField(null=True)
    create_time = models.DateField(auto_now_add=True)

#用户与文章收藏是多对多的关系,用第三张表来表示
class User2File(models.Model):
    user = models.ForeignKey(to=UserInfo, on_delete=models.CASCADE)
    file = models.ForeignKey(to=UserFile, on_delete=models.CASCADE)
    # 收藏状态
    static = models.BooleanField(null=True)
    create_time = models.DateField(auto_now_add=True)



#私信表
class User2UserWechat(models.Model):
    send = models.SmallIntegerField(verbose_name='发送者')
    recv = models.SmallIntegerField(verbose_name='接收者')
    content = models.TextField(verbose_name='聊天内容')
    create_time = models.DateTimeField(auto_now_add=True,null=True)
    status = models.IntegerField(choices=((1,'未读'),(2,'已读'),(3,'发送方删除'),(4,'接收方删除')),default=1)

#资源分类表
class Categorys(models.Model):
    categoryid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    descn = models.CharField(max_length=200)
    create_time = models.DateField(auto_now_add=True,null=True)
    userinfo = models.ForeignKey(to=UserInfo,on_delete=models.CASCADE,null=True)

#资源信息表
class Resource(models.Model):
    resourceid = models.AutoField(primary_key=True)
    categoryid = models.ForeignKey(to=Category,on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    descn = models.CharField(max_length=200)
    image = models.FileField(upload_to='resoure_img',null=True)
    code = models.FileField(upload_to='code_file',null=True)
    filename= models.CharField(max_length=32,null=True)
    is_delete=models.BooleanField(default=False)
    level = models.IntegerField(choices=((1,'高级'),(2,'中级'),(3,'初级')),default=3)
    uploader = models.ForeignKey(to=UserInfo,on_delete=models.CASCADE,null=True)
    down_numbers = models.IntegerField(default=0)
    create_time = models.DateField(auto_now_add=True,null=True)

#用户与资源的收藏是多对多的关系
class User2Source(models.Model):
    user_id = models.ForeignKey(to=UserInfo,on_delete=models.CASCADE)
    source_id =models.ForeignKey(to=Resource,on_delete=models.CASCADE)
    create_time = models.DateField(auto_now_add=True)




